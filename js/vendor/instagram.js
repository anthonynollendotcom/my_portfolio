/**
 * @author Anthony Nollen
 */

//declares jQuery object
(function($){})(window.jQuery);

/*  adds the instagram api  */
var pullInsta;

///stores the comment
var theComment;

var youUser,
    youToken;

var imageSelect = $(".instagram-load"),
    imageAmount;

var init = function(){
pullInsta = new $.ajax({
        type: "GET",
        dataType: "jsonp",
        cache: false,
        url: "https://api.instagram.com/v1/users/"+youUser+"/media/recent/"+youToken+"",
        success: function(data) {
        //logs the instagram oject
        console.log(data);

        ///gets user datd from first image object
        var userName = data.data[1].user.full_name,
            userBio = data.data[1].user.bio,
            profilePicture = data.data[1].user.profile_picture,
            userWebsite = data.data[1].user.website;

        if(data.data[1].comments.data[0]){
                theComment = data.data[theUrl].comments.data[0].text;
            }
        else{
               theComment = "No Comment" 
        }

        //displays user info
        $(".instagram-user").append("<div class='instagram-info'><div class='proPic'><img class='instagram-image' src='" + profilePicture +"' width=50/></div><div class='proCopy'><p class='name'>"+ userName +"</p><p><small>"+ userBio +"</small></p><p><a href='"+ userWebsite +"'>"+ userWebsite +"</a></p></div></div></div>");   

        //displays first main image
         imageSelect.append("<div class='big'><p>"+ theComment + "</p></div><p>"+"Likes: " + data.data[0].likes.count+"</p><div><div class='theImage'><img onload='transIN()' src='" + data.data[0].images.standard_resolution.url +"'/></div></div>");  

        ///load all the thumbs
        //all images
        if(theAmount >= data.data.length){
            theAmount = data.data.length;
        }
        for (var i = 0; i < theAmount; i++) {
            console.log(data.data[i].images.thumbnail.url);
            //gets all the images
            $(".instagram").append("<div class='instagram-placeholder'><a target='_blank' onClick='" + "loadHigh("+ i +");" + "'><img class='instagram-image' src='" + data.data[i].images.thumbnail.url +"' width=50/></a></div>");   
            }     
                            
        }
    });
    ///images: standard_resolution  _ thumbnail  _  low_resolution
    ///links to image: <a target='_blank' href='" + data.data[i].link +"'>
    //user id = 31566088
};

var loadHigh = function(theUrl){
    pullInsta = new $.ajax({
        type: "GET",
        dataType: "jsonp",
        cache: false,
        url: "https://api.instagram.com/v1/users/"+youUser+"/media/recent/"+youToken+"",
        //"https://api.instagram.com/v1/users/"+youUser+"/media/recent/"+youToken+""   for user data
        success: function(data) {
            if(data.data[theUrl].comments.data[0]){
                theComment = data.data[theUrl].comments.data[0].text;
            }
            else{
               theComment = "No Comment" 
            }
            imageSelect.fadeOut("slow", function() {imageSelect.html("<div class='big'><p>"+ theComment + "</p></div><p>"+"Likes: " + data.data[theUrl].likes.count+"</p><div class='theImage'><img onload='transIN()' src='" + data.data[theUrl].images.standard_resolution.url +"'/></div></div>");
            });
        }
    });
};

var transIN = function(){
    imageSelect.fadeIn("slow");
};

/* trigger when page is ready */
var gostagram = function(user, token, amount){
    theAmount = amount
    youUser = user;
    youToken = "?access_token=" + token + "&count=" + theAmount;
    // your functions go here
    var goInsta = new init();
};

