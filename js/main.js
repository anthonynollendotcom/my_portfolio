/*

Author: anthonynollen.com
Description: portfolio site funcitonality

                                                                                
                                                                                
                7I                                     I7   7 77                
                 777  777I7                           ,77  7:                   
                   7:7  777+                       7,77  7+777                  
                    7??    77I7,7                  ~7   7:7                     
                    777:      7 ~~77=~~77=~~77,? 777   7: 7                     
                      77: 7     7,,~~~~~~~~~~,~7      ~I                        
                         :7   7,~~,~~~~~~~~~~,~~I    : 7                        
                         7=~  ,~,,:~~~~:~~~~~,,::7 7:7                          
                           7:7,,,::::::::::::::,,?7:77                          
                           77:7:::::::::::::::::~::                             
                            7::?=~~~~~:::~~~~~~=::77                            
                            77:~~~~~~~~::~~~~~~~:77                             
                            7 7~~~~~~~~~~~~~~~~~:77                             
                              =~~~~~~~~~~~~~~~~~:77                             
                            777~~~~~~~~~~~~~~~~~:77                             
                            7::::~~~~~~~~~~~~~~:::7                             
                            7::7::~~~~~~~~~~~::7::?                             
                           7:::7~::~~~~~~~~~~::I:::I77                          
           77 7=:::::::::+I7~::7=~:~~~~~~~~~::~7?:+I+7::::::::::I 7             
       7::~7777 777777777I 7I777I~~:~~~~~~~::~~777I77777777  7 77777=::I        
 7 =7777              777777 7777~~~:~~~~~~:~~~II777I777I77             77 :77  
                    7777777777?7I~~~~:~~~~:~~~~7777777777777                    
                    77 77I777+7?7+~~~~::::~~~~7?II7777I777I7                    
                   777777777++7??77~~~~~~~~~=7???7+77777777777                  
                  777777777++7I???I7~~~~~~~~7????77+7777777 7777                
                77777777777?77I??I?777~77~I7??I??777+7777777  777               
              7777 7777777+77777777777777777777777777+77777777  777             
            7777  77 7777+7777777I?I7~~~~~~77??77  7 +II77777777 777            
           77I 7777777 77+ 7777???777777777777??I  77?+777777 I7  7777          
           777  77  7   7+ 7777777777~7?=7~777777  77I+I7  77 7I77 777          
         7 7  777777  77?+ 7  77???7~I~~=~7~?I??7  77++?77  77 77I7 7I7         
         7777777 777  77I  7    77777777777777777  777 +777 77777777777         
        77777777777 777+   7    7?7~~~~~~~~~~777    777 7777  777  777777       
        7I7777 7777 77I7   777  7I~7777777777~77   777  ?777    7777777I7       
        777I   77    I77   7I7  7I+I~~~~~~~~=?777  777   + 77    77 77I 7       
      77777  777   777     777 777~~~~~~~~~~~7  7 77     7+777    77   777      
      77777 777    7I7      77777  =~~~~~~~~    7777       +77   777    777     
      777   77    7I        7777    7=~~~~7      777        +I77   777   77     
      77   777   +77        7777      ~=~          77       7?77   777   77     
      77   777 7+777       777        77           77777      777   7777 77     
      77 77777+ 77        77                        77I77     77I7  7777777     
       7 777    777    777                            7777 7    7 7 777777      
      7777I7   77   77777                                7777777777   7777      
        I77   7777 77777                                    7777777777777       
         77777777777                                            77777777        
            7777                                                             

*/

"use strict";
////////////////////////////////////////////////
/*
///global vars keep as limited as possible
*/
////////////////////////////////////////////////

var global = this;

var siteVars = {
    bgImage: $("#bgImage"),
    theLoader: $("#loading"),
    extraBg: $("#extraFly"),
    theContent: $("#content"),
    theBody: $("#body1"),
    theBody2: $('#body2'),

    //for the fly
    flyBlit: undefined,
    switchLeft: undefined,
    switchTop: 0,
    countSwitch: 0,
    switchRotate: 270,
    flyLoaded: false,
    startPlace: undefined,
    goFlyPlace: undefined,
    flyToo: undefined,

    //for touch
    naviDiv: undefined,
    naviStartTouch: undefined,
    throwTimer: undefined,
    throwTime: undefined,
    throwStart: undefined,
    throwDist: undefined,
    mouseDif2: undefined,
    testTouch: undefined,

    //for navi bar
    naviOpen: false,
    naviBtn: $('#naviButton'),

    //for json load
    jsonOBJ: "",

    //port page
    backScroll: 0,

    ///for hashs
    currentHash: "reel",

    thePage: undefined,
    current_url_w_hash: undefined,
    checkBtns: true,
    RunTabs: undefined,

    hashedProject: undefined

};


////////////////////////////////////////////////
/*
///start everything here
*/
////////////////////////////////////////////////

var go = {
    init: function() {
        var loadHash = window.location.hash.substr(2),
            htmlHash = "",
            startHash = "";

        //checks to see if is a landing page or a dynamic detail page.
        if (loadHash === "" || loadHash === "#" || loadHash === "#undefined") {
            htmlHash = String("reel.html");
            startHash = eval("init.reelStart");
            helpers.loadHTML("#content", htmlHash, "loading", true, startHash);
        } else {
            if (loadHash === "reel" || loadHash === "work") {
                htmlHash = String(loadHash + ".html");
                startHash = eval("init." + loadHash + "Start");
                helpers.loadHTML("#content", htmlHash, "loading", true, startHash);

            } else {
                htmlHash = String("work.html");
                startHash = init.detailStart;
                helpers.loadHTML("#content", htmlHash, "loading", true, startHash);
            }
        }

        ///tracks hash for back button
        ///check for forwardback btns hash changes
        siteVars.thePage = window.location.href.substr(0, window.location.href.indexOf('#'));


        $(window).on('hashchange', hashChanged);

        function hashChanged() {
            init.globalTransOut();
        }
    },
}

var init = {
    reelStart: function() {
        helpers.checkForDevice(site.position);

        //hides loaders
        helpers.toggle_visibility("loading");
        helpers.toggle_visibility("theClientCopy");


        ///for fit js responsive video
        //sets up responsive video calls callback for ui trans in
        $(".video-container").fitVids({
            tweakAspect: 12.5,
            callBack: transitions.transIn
        });

    },

    workStart: function() {
        //hides loaders
        helpers.toggle_visibility("loading");
        helpers.toggle_visibility("theClientCopy");
        init.formatProjects(0);
        helpers.checkForDevice(site.position);
    },

    detailStart: function() {
        for (var j = 0; j < siteVars.jsonOBJ.length; j++) {
            var testHash = siteVars.jsonOBJ[j].name.replace(/[" "]+/g, "").trim();
            if (testHash == siteVars.currentHash) {
                siteVars.hashedProject = j;
            }
        }
        portPage.formatDetail(siteVars.hashedProject);
        helpers.checkForDevice(site.position);

    },

    setHashProject: function(current) {
        var theProject = current;
        return theProject;
    },

    totalProjects: siteVars.jsonOBJ.length,
    currentProjects: 0,
    showAmount: 16,
    formatProjects: function(startMargin) {
        init.totalProjects = siteVars.jsonOBJ.length;
        for (var f = 0; f < init.totalProjects; f++) {

            var theData = JSON.stringify(siteVars.jsonOBJ[f]);

            if (String(siteVars.jsonOBJ[f].type) == "FEATURE") {
                portPage.featureLink = String(siteVars.jsonOBJ[f].name);
            }
        }


        if (init.currentProjects == 0) {
            portPage.formatFeature();
        }


        for (var j = init.currentProjects; j < init.totalProjects; j++) {

            var theData = JSON.stringify(siteVars.jsonOBJ[j]);

            if (String(siteVars.jsonOBJ[j].type) != "FEATURE") {
                $('#projectsContainer').append("<div id=" + String("projContainer" + j) + ">" +
                    "<div id='projType'><h1>" + String(siteVars.jsonOBJ[j].type) + "</h1></div>");

                var theContainer = $('#projContainer' + j);

                theContainer.css('text-align', "center");
                theContainer.css('background-image', 'url(' + String(siteVars.jsonOBJ[j].bgPath) + ')');
                theContainer.css('height', "400");
                theContainer.css('width', "100%");
                theContainer.css('margin-top', startMargin + "px");

                ////limits the char count in the description for making it pretty purposes
                var limitDesc = siteVars.jsonOBJ[j].description.substring(0, 170);
                var limitSpace = limitDesc.lastIndexOf(" ");
                var finalDesc = siteVars.jsonOBJ[j].description.substring(0, limitSpace);

                var projectHash = String(siteVars.jsonOBJ[j].name).replace(/\s/g, '');

                theContainer.append("<div id='projOverlay' class='projectOff'></div><div id=" + String("projectDetail") + ">" +
                    "<div id='projName'><h3>" + String(siteVars.jsonOBJ[j].name) + "</h3></div>" +
                    "<div class='headerBoarder'></div>" +
                    "<div id='projDesc'><p class='descCopy'>" + String(finalDesc) + "..." + "</p></div>" +
                    "<div id='projLink'><a class='viewProject' href=" + "#!" + projectHash + ">View</a></div></div>");

                    $(theContainer).mouseover(function() {
                        $(this).children("#projOverlay").removeClass("projectOff").addClass("projectOn");
                    }).mouseout(function() {
                        $(this).children("#projOverlay").removeClass("projectOn").addClass("projectOff");
                    });
                } else {
                    //do nothing
                }
            }

            $('body').waitForImages(
                function() {
                    init.globalTrans();
                }
            );
        },

        globalEase: "easeInOutQuart",
        globalTrans: function() {
            $("#loading").velocity({
                opacity: 0
            }, {
                duration: 500,
                easing: "easeOutCubic",
                delay: 0
            });

            $('#transDiv').css('height', '100%');
            $('#trans1').css('height', '100%');
            $('#trans2').css('height', '100%');
            $('#trans3').css('height', '100%');
            $('#trans4').css('height', '100%');

            $("#content").css('opacity', '1');

            siteVars.naviBtn.css('opacity', '1');

            $('#trans1').velocity({
                height: "0%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 0
            });
            $('#trans2').velocity({
                height: "0%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 200
            });


            $('#trans3').velocity({
                height: "0%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 400
            });

            $('#trans4').velocity({
                height: "0%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 600,
                complete: transDone

            });

            //var delayCollpase = setTimeout(transDone, 2000);
            function transDone() {
                $('#transDiv').css('height', '0%');
                sideBar.init();
                if (String(siteVars.currentHash) === "work") {
                    //sets up the webGl scene for 3d object loads
                    my3D.init();
                }
            }
        },

        globalTransOut: function() {
            $('#transDiv').css('height', '100%');

            $('#trans4').velocity({
                height: "100%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 0
            });
            $('#trans3').velocity({
                height: "100%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 200
            });


            $('#trans2').velocity({
                height: "100%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 400
            });

            $('#trans1').velocity({
                height: "100%"
            }, {
                duration: 400,
                easing: init.globalEase,
                mobileHA: true,
                delay: 600,

            });



            var delayCollpase = setTimeout(transDone2, 2000);

            function transDone2() {
                window.location.reload();
            }
        }
    };
    ////////////////////////////////////////////////
    /*
///start everything here END
*/
    ////////////////////////////////////////////////

    ////////////////////////////////////////////////
    /*
///portPage Functions / formatting
*/
    ////////////////////////////////////////////////
    var portPage = {
        theContent: undefined,
        currentContent: 0,
        prevGo: 0,
        nextGo: 0,
        prevProj: undefined,
        nextProj: undefined,
        formatDetail: function(data) {

            portPage.currentContent = data;
            portPage.theContent = siteVars.jsonOBJ[portPage.currentContent];
            //sets the navi vars
            if (portPage.currentContent != 0) {
                portPage.prevGo = portPage.currentContent - 1;
            } else {
                portPage.prevGo = siteVars.jsonOBJ.length - 1;
            }

            if (portPage.currentContent != siteVars.jsonOBJ.length - 1) {
                portPage.nextGo = portPage.currentContent + 1;
            } else {
                portPage.nextGo = 0;
            }

            portPage.prevProj = (siteVars.jsonOBJ[portPage.prevGo].name).replace(/\s/g, '');
            portPage.nextProj = (siteVars.jsonOBJ[portPage.nextGo].name).replace(/\s/g, '');;


            formatDetail();

            function formatDetail() {
                siteVars.theContent.html("");

                siteVars.theContent.append("<div id='projectsDetailContainer'></div>");

                var theContainer = $('#projectsDetailContainer');
                theContainer.append("<div id='detailHolder'><div id=" + String("project") + ">" +
                    "<div id='projName'><h3>" + portPage.theContent.name + "</h3></div>" +
                    "<div class='headerBoarder'></div>" +
                    "<div id='projDesc'><p class='descCopy'>" + portPage.theContent.description + "</p></div></div></div");


                $('#detailHolder').css('background-image', 'url(' + String(portPage.theContent.bgPath) + ')');
                $('#detailHolder').css('width', "100%");

                theContainer.css('text-align', "center");
                theContainer.css('background-color', portPage.theContent.bgColor);
                theContainer.css('background-repeat', 'repeat');
                theContainer.css('backgroundAttachment', 'fixed');
                theContainer.css('backgroundPosition', 'top center');
                theContainer.css('min-width', "100%");

                theContainer.append("<div id='detailHeader'><div id='headerIn'><div id='prevTop'><div id='prevLink'><a href=" + "#!" + (portPage.prevProj) + " class='viewProject'>prev</a></div></div><div class='titleHeader'><h2 class='workTitle'>Work</h2></div><div id='nextTop'><div id='nextLink'><a href=" + "#!" + (portPage.nextProj) + " class='viewProject'>next</a></div></div></div></div>");

                var theHeader = $("#detailHeader");

                /// adds video embed if json data node has video!!!!!! you tube and vimeo
                if (portPage.theContent.video) {
                    theContainer.append("<div id='reelBoxDetail'><div id='projectsDetailVideo'></div></div>");
                    $('#projectsDetailVideo').append("<div class='detailVideo'>" + "<iframe src=" + portPage.theContent.video + "?portrait=0&amp;color=ff0000&amp;loop=1' width='500px' height='281px' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>");

                    //sets the header padding bgcolor
                    theHeader.css('background-color', '#330000');

                } else {
                    //sets the header padding bgcolor
                    theHeader.css('background-color', portPage.theContent.bgColor);
                }

                $('body').waitForImages().done(
                    function() {
                        init.globalTrans();
                        $('#projectsDetailContainer').css("opacity", 1);
                    }
                );

                function noCallBack() {}
                theContainer.append("<div id='projectsDetailImages'></div>");

                var projectsLoadDiv = $('#projectsDetailImages');
                var partsOfStr = portPage.theContent.images.split(',');
                var partsOfStrCopy = portPage.theContent.imageCopy.split(',');
                var sqlImages = partsOfStr;
                var sqlImagesCopy = partsOfStrCopy;

                for (var i = 0; i < sqlImages.length; i++) {
                    var removeDeploy = sqlImages[i].substring('_deploy/'.length);
                    if (sqlImagesCopy[i] != "") {
                        var splitText = sqlImagesCopy[i].split('|');
                        var header = splitText[0];
                        var body = splitText[1];
                        var copyReplace = body.replace(/_/g, ',');

                        projectsLoadDiv.append("<div class='detailImageCopy'><h3 class='imagesHeader'>" + header + "</h3><hr /><p class='descCopy'>" + copyReplace + "</p></div>");
                    }

                    projectsLoadDiv.append("<div class='detailImage'>" + "<img src='" + removeDeploy + "' alt='Anthony Nollen Chicago IL' title='Anthony Nollen Chicago IL'" + portPage.theContent.description + "' width='100%'></div>");
                }


                theContainer.append("<div class='detailFooter'><div id='prevBottom'><div id='prevLink'><a class='viewProject' href=" + "#!" + (portPage.prevProj) + ">prev</a></div></div><div id='nextBottom'><div id='nextLink'><a href=" + "#!" + (portPage.nextProj) + " class='viewProject'>next</a></div></div></div>");
                $(".detailFooter").css('background-color', portPage.theContent.bgColor);
            }
        },

        Container: $("#3dLayer"),
        stageWidth: $(window).width(),
        stageHeight: 450,
        ///lights for world
        light1: undefined,
        light2: undefined,
        light3: undefined,
        light4: undefined,
        light5: undefined,
        featureLink: undefined,
        formatFeature: function() {

            var context = helpers.webgl_detect(1);
            if (context) {
                $('#projectsContainer').append("<div id='projectsFeatured'><div id='featureCopy'><h3 id='featureH3'>DELVE</h3>" +
                    "<div id='featDesc'><div class='featureBoarder'></div><p class='descCopy'>We were asked internally to make something cool for the touch screen that lives in the Chicago Razorfish office. Utilizing html5 technologies powered by Google Chrome we created delve. I was the lead developer, ui designer, and, part of the concept team. This project was featured by Leap Motion and Adobe.</p></div>" +
                    "<div id='projLink'><a class='viewProject' href='#!" + portPage.featureLink + "'>View</a></div></div><div id='Load_Hide'></div><div id='3dLayer'><canvas id='myCanvas'></canvas></div></div></div>");
                
                $("#Load_Hide").css('position', 'absolute').css('z-index','0').css('background-color', '#045a57').css('height', '450px').css('width', '100%');

                startThree.init(portPage.Container, portPage.stageWidth, portPage.stageHeight);

                $(window).resize(function() {
                    //for three js viewport
                    startThree.viewport();
                });

            } else {
                $('#projectsContainer').append("<div id='projectsFeatured'><div id='featureCopy'><h3 id='featureH3'>DELVE</h3>" +
                    "<div id='featDesc'><div class='featureBoarder'></div><p class='descCopy'>We were asked internally to make something cool for the touch screen that lives in the Chicago Razorfish office. Utilizing html5 technologies powered by Google Chrome we created delve. I was the lead developer, ui designer, and, part of the concept team. This project was featured by Leap Motion and Adobe.</p></div>" +
                    "<div id='projLink'><a class='viewProject' href='#!Delve'>View</a></div></div><div id='3dLayer'><img src='img/clients/delve/projectBack.jpg' height='450px' title='Anthony Nollen' alt='Anthony Nollen' /></div></div><div id='Load_Hide'></div>");
            }
        }

    };

    var my3D = {
        clouds: new Array(),
        cloudAmount: 150,
        earthContainers: new Array(),
        sceneObjects: new Array(),
        featureParts: new Array(),
        ParticlesStore: new Array(),
        mouseMath: 0,
        goMouse: 0,
        delayAdd: undefined,

        init: function() {

            ///max four lights change max in threeOjs.js
            var light1 = new lights.direction(0xffffff, .5, 0, 1000, 0);
            var light4 = new lights.point(0x94edd9, .1, 300, -200, 0);
            var texture = THREE.ImageUtils.loadTexture('img/feature/skyTexture.jpg');
            var atmoMat = new materials.phong(0x06d2d0, 0x009b99, 0x009b99, 100, texture);
            var atmosphere = new primitives.sphere(0xffffff, 1, my3D.sceneObjects, atmoMat);
            atmosphere.overdraw = true;

            my3D.sceneObjects[0].position.set(0, 0, 0);
            my3D.sceneObjects[0].rotation.set(0, 0, 0);
            my3D.sceneObjects[0].scale.x = my3D.sceneObjects[0].scale.y = my3D.sceneObjects[0].scale.z = 25000;
            addRemove.add(my3D.sceneObjects[0]);
            extras.addFog(0x888888, 0, 60000);

            my3D.addObjects();
        },

        addObjects: function() {
            var cloudContainer = new extras.container(my3D.earthContainers);
            var partContainer = new extras.container(my3D.earthContainers);

            my3D.featureParts = jQuery.extend({}, particale);
            my3D.featureParts.pushItIndex = 0;
            my3D.featureParts.startParticle("img/feature/driveStarMap.png",
                250, 20, 0xffffff, true, my3D.earthContainers[0],
                my3D.animateParticle, 4000,
                1000, 4000);

            my3D.ParticlesStore[my3D.featureParts.pushItIndex] = my3D.featureParts.particleSystem;

            /////adds the clouds to array earth.earthClouds for animation
            for (var i = my3D.cloudAmount; i >= 0; i--) {
                var thisCloud = Math.floor(Math.random() * 3) + 1;
                var cloudScale = Math.random() * 2;
                window["cloud" + i] = new primitives.DAE("img/feature/IslandCloud" + thisCloud + ".dae", cloudScale, my3D.clouds, i);
            }

            ///cloud container
            addRemove.add(my3D.earthContainers[0]);
            my3D.earthContainers[0].position.set(0, 0, 0);

            //cloud pre-loader
            my3D.delayAdd = setInterval(testLoads, 20);

            function testLoads() {
                if (primitives.monitor === 0) {
                    clearInterval(my3D.delayAdd);
                    my3D.renderObjects();
                    my3D.showClouds();
                }
            }

        },
        
        showClouds: function(){
            $("#Load_Hide").velocity({
                height: 0
            }, {
                duration: 400,
                delay: 0,
                easing: init.globalEase,
                mobileHA: true
            });
        },

        renderObjects: function() {
            //add number of clouds according to cloudness data.js
            for (var i = my3D.cloudAmount; i >= 0; i--) {
                my3D.earthContainers[0].add(my3D.clouds[i]);
                my3D.clouds[i].position.set((Math.random() * 30000) - 15000, (Math.random() * 1800) - 900, (Math.random() * 20000) - 10000);
                my3D.clouds[i].rotation.set(0, 0, 0);
            }

            goAnimate.animaFunction = my3D.animateIt;

            goAnimate.playPauseAnimate();

        },

        animateIt: function() {
            my3D.mouseMath = helpers.myWidth * .5;
            my3D.goMouse = (extras.getMouse.mouseX - my3D.mouseMath) * .00001;


            /*if(my3D.goMouse !== NaN){
            my3D.earthContainers[0].rotation.y += my3D.goMouse;
        }
        else{
           my3D.earthContainers[0].rotation.y += .001; 
        }*/

            my3D.earthContainers[0].rotation.y += .001;

            my3D.animateParticle();
        },

        animateParticle: function() {
            //myObjects.myParticles[0].position.y -= .3;
            var pCount = my3D.ParticlesStore[0].geometry.vertices.length;
            while (pCount--) {
                // get the particle
                var particle = my3D.ParticlesStore[0].geometry.vertices[pCount];
                // check if we need to reset
                if (particle.y <= -1500) {
                    particle.y = 1500;
                } else {
                    particle.y -= pCount / 25;
                }

            }

            // flag to the particle system
            // that we've changed its vertices.
            my3D.ParticlesStore[0].geometry.__dirtyVertices = true;
            my3D.ParticlesStore[0].geometry.__dirtyNormals = true;

        },

        kill3D: function() {
            goAnimate.playPauseAnimate();
            goAnimate.animaFunction = undefined;
            my3D.earthContainers = new Array();
            my3D.clouds = new Array();
            my3D.sceneObjects = new Array();
            my3D.featureParts = new Array();
        }
    }

    ////////////////////////////////////////////////
    /*
///page navigation END
*/
    ////////////////////////////////////////////////



    ////////////////////////////////////////////////
    /*
///custom visiual effects scripts
*/
    ////////////////////////////////////////////////
    var site = {
        reel: $("#reelBox"),
        reelCopy: $("#theClientCopy"),
        position: function() {
            site.reel = $("#reelBox");
            site.reelCopy = $("#theClientCopy");

            helpers.getScreen();

            /// this needs test for current page loaded!!!!!!!
            if (siteVars.currentHash === "reel") {
                var currentHeight = site.reel.offsetHeight;
                var currentWidth = site.reel.offsetWidth;
                site.reel.css("left", 1002 + ((helpers.myWidth / 2) - (currentWidth / 2)) + "px");

                ///for the content description
                site.reelCopy.css("top", ((helpers.myHeight) - 60) + "px");
                site.reelCopy.css("left", (helpers.myWidth) - (parseInt(site.reelCopy.width(), 10) + 20) + "px");
            }
        }
    };

    ////////////////////////////////////////////////
    /*
///page / page element transitions custom animations
*/
    ////////////////////////////////////////////////

    var transitions = {
        transIn: function() {
            //sets bgimage after load
            siteVars.bgImage = $("#bgImage");

            siteVars.bgImage.velocity({
                opacity: .5
            }, {
                duration: 1000,
                delay: 0,
                easing: "linear",
                mobileHA: true,
                complete: transitions.videoSet
            });


            ///for the reel temp trans in
            var setDiv = $("#reelBox");
            var currentHeight = setDiv.height();
            var currentWidth = setDiv.width();
            var animateThis = ((helpers.myWidth / 2) - (currentWidth / 2));

            setDiv.css("marginLeft", animateThis + "px");
            setDiv.css("marginTop", 130 + "px");

            setDiv.velocity({
                opacity: 1,
                scaleX: 1,
                scaleY: 1,
            }, {
                duration: 500,
                delay: 1200,
                mobileHA: true,
                easing: "linear",
                complete: player.init
            });


            function videoSet() {
                site.position();
                player.init();
            }

            $('body').waitForImages(
                function() {
                    init.globalTrans();
                }
            );
        },

        transOut: function() {

        },
        z: 10,
        swithcBg: function(current, prev) {
            current.velocity({
                opacity: 0,
            }, {
                duration: 500,
                delay: 0,
                easing: "linear",
                mobileHA: true
            });

            prev.velocity({
                opacity: 1,
            }, {
                duration: 500,
                delay: 0,
                easing: "linear",
                mobileHA: true
            });

        }
    };


    ////////////////////////////////////////////////
    /*
///for sprite sheet fly animation
*/
    ////////////////////////////////////////////////
    var fly = {
        init: function() {
            switch (siteVars.goFlyPlace) {
                case 0:
                    siteVars.extraBg.style.left = (Math.random() * helpers.myWidth) + "px";
                    siteVars.extraBg.style.top = (helpers.myHeight + 40) + "px";
                    $(siteVars.extraBg).rotate(0, 'abs');
                    break;
                case 1:
                    siteVars.extraBg.style.left = (Math.random() * helpers.myWidth) + "px";
                    siteVars.extraBg.style.top = ((helpers.myHeight - helpers.myHeight) - 40) + "px";
                    $(siteVars.extraBg).rotate(180, 'abs');
                    break;
                case 2:
                    siteVars.extraBg.style.left = (helpers.myWidth + 40) + "px";
                    siteVars.extraBg.style.top = (Math.random() * helpers.myHeight) + "px";
                    $(siteVars.extraBg).rotate(270, 'abs');
                    break;
                case 3:
                    siteVars.extraBg.style.left = ((helpers.myWidth - helpers.myWidth) - 40) + "px";
                    siteVars.extraBg.style.top = (Math.random() * helpers.myHeight) + "px";
                    $(siteVars.extraBg).rotate(90, 'abs');
                    break;
                default:
                    console.log("this broke, oooooohhhhhh nooooooooooo");
            }
        },

        addSplat: function() {
            //hides the image load so you dont see a flicker
            siteVars.extraBg = document.getElementById("extraFly");
            siteVars.extraBg.style.left = (-200) + "px";
            siteVars.extraBg.style.top = (-200) + "px";

            fly.loadImg('img/gifs/neonFly.png', "#extraBgIn", animateThis);
            $(siteVars.extraBg).mousedown(fly.splatFly);

            function animateThis() {
                siteVars.flyBlit = setTimeout(fly.switchImage, 60);
            }
        },

        setFlyStart: function() {
            siteVars.startPlace = Math.round(Math.random() * 3);
            return siteVars.startPlace;
        },

        splatFly: function(e) {
            var theFly = siteVars.extraBg;
            $(theFly).stop();

            clearInterval(siteVars.flyBlit);

            document.getElementById("extraBgIn").style.left = 0 + "px";
            document.getElementById("extraBgIn").style.top = -100 + "px";

            theFly.velocity({
                opacity: 0
            }, {
                duration: 0,
                delay: 2000,
                easing: "linear",
                mobileHA: true,
                complete: fly.removeFly
            });

            e.preventDefault();
        },

        countSmash: function() {
            if (siteVars.smashCount != 6) {
                siteVars.smashCount = siteVars.smashCount + 1;
            }
        },

        switchImage: function() {
            var theTop;
            helpers.getScreen();
            var theHolder = document.getElementById("extraFly");
            siteVars.countSwitch += 1;
            siteVars.switchLeft = (siteVars.countSwitch) * (-50);
            siteVars.switchTop += 3;
            if (siteVars.goFlyPlace === 0) {
                theTop = ((helpers.myHeight + 40) - siteVars.switchTop);
                theHolder.style.top = theTop + 'px';
            } else if (siteVars.goFlyPlace == 1) {
                theTop = (((helpers.myHeight - helpers.myHeight) - 40) + siteVars.switchTop);
                theHolder.style.top = theTop + 'px';
            } else if (siteVars.goFlyPlace == 2) {
                theTop = (((helpers.myWidth) + 40) - siteVars.switchTop);
                theHolder.style.left = theTop + 'px';
            } else if (siteVars.goFlyPlace == 3) {
                theTop = (((helpers.myWidth - helpers.myWidth) - 40) + siteVars.switchTop);
                theHolder.style.left = theTop + 'px';
            }
            document.getElementById("extraBgIn").style.left = (siteVars.switchLeft) + "px";
            if (siteVars.countSwitch == 9) {
                siteVars.countSwitch = 0;
                if (document.getElementById("extraBgIn").style.top == -50) {
                    document.getElementById("extraBgIn").style.top = (-50) + "px";
                } else {
                    document.getElementById("extraBgIn").style.top = 0 + "px";
                }
            }
            ///checks if fly is off screen
            fly.removeFly();
        },

        removeFly: function() {
            if (siteVars.goFlyPlace === 0 || siteVars.goFlyPlace == 1) {
                siteVars.flyToo = helpers.myWidth + 100;
            } else {
                siteVars.flyToo = helpers.myWidth + 100;
            }
            if (siteVars.switchTop >= siteVars.flyToo) {
                clearInterval(siteVars.flyBlit);
                $(siteVars.extraBgIn).find("img").remove();
                siteVars.flyLoaded = false;
            } else {
                siteVars.flyBlit = setTimeout(fly.switchImage, 10);
            }
        },

        loadImg: function(theSrc, TheContainer, CallBack) {
            if (siteVars.flyLoaded === false) {
                siteVars.goFlyPlace = fly.setFlyStart();

                siteVars.switchTop = 0;
                siteVars.countSwitch = 0;
                $(siteVars.extraBg).css({
                    opacity: 1
                });
                //sets the flys init pos
                var img = $("<img />").attr('src', theSrc)
                    .load(function() {
                        if (!this.complete) {
                            alert('broken image!');
                        } else {
                            siteVars.flyLoaded = true;
                            $(TheContainer).append(img);
                            ///sets the flys start position
                            fly.init();
                            if (CallBack) {
                                CallBack();
                            }
                        }
                    });
            }
        }
    };

    ////////////////////////////////////////////////
    /*
///for the navigation bar
*/
    ////////////////////////////////////////////////

    var sideBar = {
        theContent: $("#content"),
        theBar: $("#naviIn"),
        theSprite: document.getElementById("naviButtonImage"),
        spriteTimer: undefined,
        setMargin: 0,
        naviOpen: false,
        init: function() {
            $("#naviButton").click(function() {
                if (siteVars.naviOpen === true) {
                    sideBar.slideIn();
                    siteVars.naviOpen = false;
                } else {
                    sideBar.slideOut();
                    siteVars.naviOpen = true;
                }
            });
            sideBar.loadNavi();
        },

        loadNavi: function() {
            $("#naviBar").append("<div id='naviContainer'><div id='naviIn'><div id='linkContainer'><div class='sideLink naviBarHide'><a href='#!reel'>Reel</a></div><div class='sideLink naviBarHide'><a href='#!work'>Work</a></div><div class='sideLink naviBarHide'><a href='http://www.linkedin.com/in/anthonynollen' target='_blank'>Linkedin</a></div><div class='sideLink naviBarHide'><a href='mailto:anthonynollen@gmail.com?subject=Inquiry from anthonynollen.com' target='_blank'>Contact</a></div></div></div></div>");
            //sets the sidebar div
            sideBar.theBar = $("#naviIn");
        },

        slideIn: function() {
            sideBar.naviOpen = false;

            var waitFade = setTimeout(resizeMenu, 250);

            function resizeMenu() {
                $("#naviBar").css('width', "0px");
                $("#naviBar").css('height', "0px");

                $("#extraBg").css('width', "45px");
                $("#extraBg").css('height', "45px");
            }
            
            $('.sideLink.naviBarShow').each(function(i, obj) {
                var theDelay = i * 0;
                goNaviAnimation($(this), theDelay);
            });
            
            function goNaviAnimation(theElement, theDelay){
                var naviDelay = setTimeout(function(){
                sideBar.animateNaviIn(theElement)}, theDelay);
            }
            
            sideBar.theContent.addClass("contentHide");
            sideBar.theContent.removeClass("contentShow");

            clearInterval(sideBar.spriteTimer);
            sideBar.spriteTimer = setInterval(sideBar.spriteIn, 25);
        },
        
        animateNaviIn: function(theElement){
            theElement.removeClass("sideLink naviBarShow");
            theElement.addClass("sideLink naviBarOut");
            var naviDelay = setTimeout(function(){
                theElement.removeClass("sideLink naviBarOut");
                theElement.addClass("sideLink naviBarHide");}, 400);
        },
        
        animateNaviOut: function(theElement){
            theElement.removeClass("sideLink naviBarHide");
            theElement.addClass("sideLink naviBarShow");  
        },

        slideOut: function() {
            $('.sideLink.naviBarHide').each(function(i, obj) {
                var theDelay = ( i * 100 );
                goNaviAnimationOut($(this), theDelay);
            });
            
            function goNaviAnimationOut(theElement, theDelay){
                var naviDelay = setTimeout(function(){
                sideBar.animateNaviOut(theElement)}, theDelay);
            }
            
            sideBar.theContent.addClass("contentShow");
            sideBar.theContent.removeClass("contentHide");

            $("#extraBg").css('width', String(helpers.myWidth) + "px");
            $("#extraBg").css('height', String(helpers.myHeight) + "px");
            $("#naviBar").css('width', String(helpers.myWidth - 50) + "px");
            $("#naviBar").css('height', String(helpers.myHeight) + "px");

            clearInterval(sideBar.spriteTimer);
            sideBar.spriteTimer = setInterval(sideBar.spriteOut, 25);
        },

        spriteOut: function() {
            sideBar.setMargin = sideBar.setMargin - 46;
            sideBar.theSprite.style.left = sideBar.setMargin + 'px';
            if (sideBar.setMargin <= -250) {
                clearInterval(sideBar.spriteTimer);
                sideBar.naviOpen = true;
            }
        },

        spriteIn: function() {
            sideBar.setMargin = sideBar.setMargin + 46;
            sideBar.theSprite.style.left = sideBar.setMargin + 'px';
            if (sideBar.setMargin === 0) {
                clearInterval(sideBar.spriteTimer);
            }
        }

    };

    ////////////////////////////////////////////////////////////
    /*
///reel type effect
*/
    ////////////////////////////////////////////////////////////

    var typeIt = {
        container: $("#theClientCopy"),
        storeContent: undefined,
        currentContent: undefined,
        contentCount: undefined,
        saveContent: undefined,
        descCount: undefined,
        contentLength: undefined,
        contentTimer: undefined,
        currentCount: undefined,
        randomColors: new Array('#08FFFF', '#45FF00', '#FF00BD', '#FF0000', '#FFE600'),
        start: function(content, description) {
            typeIt.container.css("width", 350 + "px");
            typeIt.contentCount = content.length;
            typeIt.descCount = description.length;
            typeIt.contentLength = content.length + description.length + 1;
            typeIt.storeContent = String(content + description);
            typeIt.saveContent = content;

            typeIt.currentCount = 1;

            var newColor = typeIt.randomColors[Math.round(Math.random() * (typeIt.randomColors.length - 1))];


            typeIt.container.velocity({
                backgroundColor: newColor
            }, {
                duration: 500,
                delay: 0,
                mobileHA: true,
                easing: "linear"
            });

            typeIt.goContent();
            site.position();
        },

        goContent: function() {
            if (typeIt.currentCount <= typeIt.contentLength) {
                typeIt.currentCount = typeIt.contentLength;
                typeIt.currentContent = typeIt.storeContent.substring(0, typeIt.currentCount);
                typeIt.container.html("<div id='clientCotain'><p><b>" + typeIt.saveContent + "</b>" + typeIt.currentContent.substring(typeIt.contentCount, typeIt.currentCount) + "</p></div>");
                typeIt.container.css("width", parseInt(typeIt.contentLength * 8) + "px");
                typeIt.container.css("left", (helpers.myWidth) - (parseInt(typeIt.container.clientWidth + 20, 10)) + "px");
            }
        }
    };

    ////////////////////////////////////////////////
    /*
///vimeo player scripts
*/
    ////////////////////////////////////////////////
    ////vimeo cue points based on video current time

    var player = {
        iframe: undefined,
        player: undefined,
        currentDiv: undefined,
        prevDiv: undefined,
        oneTime: false,

        init: function() {
            player.currentDiv = "#bgImage";
            player.prevDiv = $("#bgImage");
            player.iframe = $('#player1')[0];
            player.player = $f(player.iframe);

            player.loadVideo();
        },

        CurrentTime: new Array(0, 5, 8, 13, 16.4, 18.2, 20.3, 23.2, 27, 29, 32.6, 37.5, 42, 44.4, 47.6, 48.6, 52, 55.5, 61.1, 65.7, 69, 73.9, 77.2, 80.1, 83, 88, 95),
        CurrentClient: new Array("REEL ", "BlackBerry ", "Bank Of America ", "Lunchables ", "Compre ", "Fuse TV ", "Dominion ", "4iphoto ", "Navistar ", "Blackberry Torch ", "RazorFish ", "Office Depot ", "Nabisco ", "Fuse TV ", "BlackBerry Torch ", "Lunchables ", "Bank Of America ", "RazorFish ", "Anthony Nollen ", "Dominion Resources ", "4iphoto ", "comicvolt.com ", "Lunchables ", "Lunchables ", "Oreo Cakesters ", "The End "),
        CurrentType: new Array("// 2009-2015", "// product landing page", "// healthcare presentation", "// microsite", "// microsite", "// microsite landing page", "// healthcare portal", "// Microsite", "// Healthcare presentation", "// Rich Media", "// webGL/leap motion display", "// healthcare presentation", "// Sweepstakes Microsite", "// microsite landing page", "// landing page", "// Microsite", "// healthcare presentation", "// webGL/leap motion display", "// personal portfolio", "// healthcare portal", "// microsite", "// comic reader microsite", "// microsite", "// sweepstakes microsite", "// facebook application", "The End ", "// have a great day"),
        CurrentImage: new Array("bgImage", "bgImage2", "bgImage3", "bgImage4", "bgImage5", "bgImage12", "bgImage11", "bgImage10", "bgImage11", "bgImage2", "bgImage8", "bgImage11", "bgImage7", "bgImage12", "bgImage2", "bgImage9", "bgImage11", "bgImage8", "bgImage12", "bgImage11", "bgImage10", "bgImage6", "bgImage4", "bgImage9", "bgImage11", "bgImageEnd"),
        CurrentProject: 0,
        checkRecipeTime: function(theTime) {
            for (var i = 0; i < player.CurrentTime.length - 1; i++) {
                if (theTime >= player.CurrentTime[i]) {
                    if (i != 0) {
                        player.CurrentProject = i;
                    } else {
                        player.CurrentProject = 0;
                    }
                }
            }

            var setCurrentImage = String("#" + player.CurrentImage[player.CurrentProject]);

            if (player.prevDiv.selector != setCurrentImage) {
                player.oneTime = true;
            }

            player.currentDiv = player.prevDiv;
            player.prevDiv = $(setCurrentImage);

            if (player.oneTime === true) {
                typeIt.start(player.CurrentClient[player.CurrentProject], player.CurrentType[player.CurrentProject]);
                if (player.CurrentProject % 3 === 0) {
                    player.vimeoFly();
                }
                transitions.swithcBg(player.currentDiv, player.prevDiv);
                player.oneTime = false;
            }
        },

        onPlayProgress: function() {
            player.player.api("getCurrentTime", function(value, player_id) {
                player.checkRecipeTime(value);
            });
        },

        onPause: function() {
            clearInterval(player.playerTimer);
            player.prevDiv.velocity({
                opacity: .5
            }, {
                duration: 500,
                delay: 0,
                mobileHA: true,
                easing: "linear"
            });
        },
        playerTimer: undefined,
        onPlay: function() {
            player.playerTimer = setInterval(player.onPlayProgress, 1);
            typeIt.start(player.CurrentClient[player.CurrentProject], player.CurrentType[player.CurrentProject]);
            player.prevDiv.velocity({
                opacity: 1
            }, {
                duration: 500,
                delay: 0,
                mobileHA: true,
                easing: "linear",
                complete: site.position
            });
        },

        onFinish: function() {
            player.prevDiv.velocity({
                opacity: .5
            }, {
                duration: 500,
                delay: 0,
                mobileHA: true,
                easing: "linear"
            });
        },

        vimeoFly: function() {
            fly.addSplat();
        },

        loadVideo: function() {
            //player.player.addEvent('playProgress', player.onPlayProgress);
            player.player.addEvent('pause', player.onPause);
            player.player.addEvent('play', player.onPlay);
            player.player.addEvent('finish', player.onFinish);
            // player.player.api("play");
        },
    };


    ////////////////////////////////////////////////
    /*
///vimeo player END
*/
    ////////////////////////////////////////////////

    ////////////////////////////////////////////////
    /*
///custom visiual effects scripts END
*/
    ////////////////////////////////////////////////





    //////////////////////////////////////////////////////////////////////
    /*
Helpers
*/
    //////////////////////////////////////////////////////////////////////
    var helpers = {
        ////////////
        //loads HTML into file
        ////////////
        loadHTML: function(container, page, loader, scroll, callback) {
            //transtion pages
            if (loader) {
                helpers.toggle_visibility(loader);
            }
            $(String(container)).load(String(page), function() {
                $("#content").css('opacity', '0');
                if (scroll) {
                    $('body').css('overflow-y', 'visible');
                    $('#body1').css('overflow-y', 'visible');
                }
                if (!scroll) {
                    $('body').css('overflow-y', 'hidden');
                    $('#body1').css('overflow-y', 'hidden');
                }
                ///
                if (callback) {
                    var myLoader = $("#" + loader);
                    myLoader.velocity({
                        opacity: 1
                    }, {
                        duration: 500,
                        easing: "linear",
                        delay: 2000,
                        complete: callback
                    });

                }
            });
            ///get screen dem
            helpers.getScreen();
            ///sets the hash
            siteVars.current_url_w_hash = document.location;
            ///allows for back btns with hash
        },

        ////////////
        //loads javascript code into this file
        ////////////
        loadScript: function(url, callback) {
            // adding the script tag to the head as suggested before
            var head = document.getElementsByTagName('head')[0],
                script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = url;

            // then bind the event to the callback function
            // there are several events for cross browser compatibility
            script.onreadystatechange = callback;
            script.onload = callback;

            // fire the loading
            head.appendChild(script);
        },

        ////////////
        //loads JSON
        ////////////
        loadJSON: function(file, callBack) {
            //container: "#div id"
            //page: html file path
            //loader: div to show while loading file
            //callback: function name
            var jsonURL = String(file);
            $.getJSON(jsonURL, function(json) {
                siteVars.jsonOBJ = json.projects;
                callBack();

            });
        },

        ////////////
        //Returns current html file name
        ////////////
        returnDocumentName: function() {
            var file_name = document.location.href;
            var end = (file_name.indexOf("?") === -1) ? file_name.length : file_name.indexOf("?");
            return file_name.substring(file_name.lastIndexOf("/") + 1, end);
        },

        ////////////
        //hides and shows divs id=String
        ////////////
        toggle_visibility: function(id) {
            var e = document.getElementById(id);
            if (id !== undefined) {
                if (e.style.display === 'block') {
                    e.style.display = 'none';
                } else {
                    e.style.display = 'block';
                }
            }
        },

        ////////////
        //function set URl deep linking
        ////////////
        setTheUrl: function(set) {
            //if (hashSet === true) {
            var splitHash = set.indexOf(".");
            if (splitHash >= 0) {
                var setHash = set.substring(0, splitHash);
                global.location = "#" + (setHash.toString());
            } else {
                var splitHash = set.replace(/[" "]+/g, "").trim();
                global.location = "#" + (splitHash.toString());
            }
            // }
        },

        ////////////
        //for checking hash changes
        ////////////
        trackHash: function() {
            ///change 'http://www.yoursite.com/'
            var page_url = 'http://port/_build/'; // full path leading up to hash;
            // page_url + window.location.hash
            var current_url_w_hash = window.location.hash; // now you might have something like: 
            // document.location != page_url + current_url_w_hash
            if (document.location !== page_url + current_url_w_hash) {
                window.location = document.location;
            }
            return false;
        },

        ////////////
        //for checking hash in location
        ////////////
        getDeep: function() {
            var hash = global.location.hash;
            if (global.location.hash !== "") {
                siteVars.currentHash = hash.substring(2);
                siteVars.currentHash = siteVars.currentHash;
            } else {
                siteVars.currentHash = siteVars.currentHash;
            }
        },


        ////////////
        //set session storage
        ////////////
        setStorage: function(theValue, theKey) {
            var stringIt = theValue.toString();
            myStorage.setItem(theKey, stringIt);
        },

        ////////////
        //get session storage
        ////////////
        getStorage: function(theKey) {
            var theVar;
            if (myStorage.length !== 0) {
                theVar = myStorage.getItem(theKey);
            }
            return theVar;
        },

        ////////////
        //get distance between two things - DIV, Numbers in general
        ////////////
        distance: function(a, b) {
            return Math.abs(a - b);
        },

        ////////////
        //sets window current height and width
        ////////////
        myWidth: 0,
        myHeight: 0,
        getScreen: function() {
            if (typeof($(global).width()) === 'number') {
                //Non-IE
                helpers.myWidth = $(global).width();
                helpers.myHeight = $(global).height();
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                //IE 6+ in 'standards compliant mode'
                helpers.myWidth = document.documentElement.clientWidth;
                helpers.myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                //IE 4 compatible
                helpers.myWidth = document.body.clientWidth;
                helpers.myHeight = document.body.clientHeight;
            }
        },

        /// this is not working now?????????????
        is_touch_device: function() {
            if (global.Touch) {
                return true;
            } else {
                return false;
            }
        },

        checkForDevice: function(myfunction) {
            var checkFunction = myfunction;
            if (siteVars.testTouch === true) {
                global.onorientationchange = function(event) {
                    helpers.getScreen();
                    checkFunction();
                };
            } else {
                global.onresize = function(event) {
                    //temp
                    if (sideBar.naviOpen) {
                        $("#naviBar").css('width', String(helpers.myWidth - 50) + "px");
                        $("#naviBar").css('height', String(helpers.myHeight) + "px");
                        $("#extraBg").css('width', String(helpers.myWidth) + "px");
                        $("#extraBg").css('height', String(helpers.myHeight) + "px");
                    }
                    //checks if the window deminsions are to small
                    helpers.getScreen();
                    checkFunction();
                };
            }
        },

        webgl_detect: function(return_context) {
            if ( !! window.WebGLRenderingContext) {
                var canvas = document.createElement("canvas"),
                    names = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],
                    context = false;

                for (var i = 0; i < 4; i++) {
                    try {
                        context = canvas.getContext(names[i]);
                        if (context && typeof context.getParameter == "function") {
                            // WebGL is enabled
                            if (return_context) {
                                // return WebGL object if the function's argument is present
                                return {
                                    name: names[i],
                                    gl: context
                                };
                            }
                            // else, return just true
                            return true;
                        }
                    } catch (e) {}
                }

                // WebGL is supported, but disabled
                return false;
            }

            // WebGL not supported
            return false;
        },

        getScrollXY: function() {
            var x = 0,
                y = 0;
            if (typeof(window.pageYOffset) == 'number') {
                // Netscape
                x = window.pageXOffset;
                y = window.pageYOffset;
            } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                // DOM
                x = document.body.scrollLeft;
                y = document.body.scrollTop;
            } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                // IE6 standards compliant mode
                x = document.documentElement.scrollLeft;
                y = document.documentElement.scrollTop;
            }
            return [y];
        }

    };

    //////////////////////////////////////////////////////////////////////
    /*
Helpers END
*/
    //////////////////////////////////////////////////////////////////////

    global.onload = function() {
        //adds resize listener
        //helpers.is_touch_device
        siteVars.testTouch = false;

        helpers.getDeep();

        helpers.loadJSON('data/portData.json', go.init);

    }